import telnetlib
import multiprocessing

def poke_host(hostname, port=5900, timeout=10):
    tn = telnetlib.Telnet()
    try:
        tn.open(hostname, port, timeout)
    except Exception as e:
        #We probably timed out, or perhaps hostname didnt exist
        result = 1
    else:
        #Successfully connected to host
        result = 0
    finally: 
        tn.close()
    return result

def poke_concurrent(host_list):
    pool = multiprocessing.Pool(processes=15)
    results = zip(host_list, pool.map(poke_host,host_list))
    pool.close()
    pool.join()
    return results

