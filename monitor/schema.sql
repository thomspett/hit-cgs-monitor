drop table if exists hosts;
create table hosts (
    id integer primary key autoincrement,
    hostname text not null,
    roomname text not null
);

drop table if exists results;
create table results (
    id integer primary key autoincrement,
    time_id integer not null,
    result integer not null,
    host_id integer not null,
    FOREIGN KEY(host_id) REFERENCES hosts(id),
    FOREIGN KEY(time_id) REFERENCES times(id)
);

drop table if exists times;
create table times (
    id integer primary key autoincrement,
    time datetime not null
);