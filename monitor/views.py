from datetime import datetime, timedelta
from collections import defaultdict

from flask import render_template, redirect, url_for, request, flash

from monitor import app
from db_stuff import query_db, insert_db

@app.route('/')
def show_hosts():
    entries = query_db('SELECT roomname, hostname FROM hosts ORDER BY roomname ASC')
    return render_template('show_hosts.html', entries=entries)

def new_host(hostname, roomname):
    insert_db('hosts', ['hostname', 'roomname'], \
            [hostname, roomname])

@app.route('/add_host', methods=['GET', 'POST'])
def add_host():
    if request.method == 'POST':
        new_host(request.form['hostname'], request.form['roomname'])
        flash('New host added')
        return redirect(url_for('add_host'))
    return render_template('add_host.html')

def get_pokelog(hours=24):
    start = datetime.now()-timedelta(hours=hours)
    times = query_db('SELECT id, time FROM times \
            WHERE time > (?) ORDER BY time ASC', (str(start),))
    hosts = query_db('SELECT id, hostname, roomname from hosts \
            ORDER BY roomname ASC')
    if len(times) > 0:
        earliest = times[0]['id']
        results = query_db('SELECT time_id, result, host_id FROM results \
                WHERE time_id >= (?) ORDER BY host_id, time_id', (earliest,))
    else:
        results = []
    return (times, hosts, results)

def make_table(times, hosts, results):
    table = defaultdict(dict)
    for result in results:
        table[result['host_id']][result['time_id']] = result['result']
    return table

@app.route('/monitor/')
@app.route('/monitor/<int:hours>')
def show_pokelog(hours=24):
    (times, hosts, results) = get_pokelog(hours)
    table = make_table(times, hosts, results)
    return render_template('monitor.html', \
            times=times, hosts=hosts, results=table)


@app.template_filter()
def goodtimes(s):
    return s[:-7]

@app.template_filter()
def avd(s):
    return s[0:2]
