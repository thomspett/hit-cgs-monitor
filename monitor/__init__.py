import os

from flask import Flask

app = Flask(__name__)
app.config.from_object(__name__)

app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'monitor.db'),
    SECRET_KEY='hemmelig',
    DEBUG=None
))

import monitor.views
