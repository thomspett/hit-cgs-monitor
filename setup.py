from setuptools import setup

setup(
	name='HIT Monitor',
	version='0.1',
	long_description=__doc__,
	packages=['monitor'],
	include_package_data=True,
	zip_safe=False,
	install_requires=[
		"Flask==0.10.1", 
		"Flask-Script==2.0.5"
	]
)
