#!/usr/bin/env python

import csv
from datetime import datetime

from flask.ext.script import Manager, Shell, Server

from monitor import app 
from monitor.db_stuff import query_db, get_db, insert_db, init_db
from monitor.poke import poke_concurrent

manager = Manager(app)

@manager.command
def poke(repeat):
    with app.app_context():
        hosts = query_db('SELECT hostname FROM hosts')
        host_list = [h['hostname'] for h in hosts]
        time_id = insert_db('times', ['time'], [datetime.now()])
        results = []
        for i in range(int(repeat)):
            pokes = poke_concurrent(host_list)
            if i == 0:
                results = [(h, r, time_id) for (h, r) in pokes]
            else:
                results = [(h, r+s, t) for (h, r, t), (_, s) in zip(results, pokes)]
            
        db = get_db()
        cur = db.executemany('INSERT INTO "results" (host_id, result, time_id) VALUES \
                ((SELECT id FROM hosts WHERE hostname = ?), ?, ?)', results)
        db.commit()
        cur.close()

@manager.command
def import_hosts():
    hosts = []
    with open('fixtures/hosts.csv', 'r') as csvfile:
        hostreader = csv.reader(csvfile, delimiter=';', quotechar='"')
        for row in hostreader:
            hosts.append(row)
    db = get_db()
    cur = db.executemany('INSERT INTO "hosts" (roomname, hostname) VALUES (?, ?)', hosts)
    db.commit()
    cur.close()

@manager.command
def db_init():
    init_db()


manager.add_command("runserver", Server(host="0.0.0.0"))

manager.add_command("shell", Shell())

manager.run()
